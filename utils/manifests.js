const path = require('path');
const fs = require('fs');

module.exports = (config) => {
  const manifestDir = path.join(__dirname, '..', 'manifests');
  if (config.mode) {
    const getManifests = (mode) => {
      const manifestPath = path.join(manifestDir, `${mode}.json`);
      if (!fs.existsSync(manifestPath)) throw new Error(`Unable to find ${mode}, with path: ${manifestPath}`);
      return require(manifestPath);
    };
    switch (typeof config.mode) {
      case 'string':
        return [getManifests(config.mode)];
      case 'object':
        return Object.values(config.mode)
          .map((mode) => getManifests(mode));
      default:
        throw new Error('unknown config.mode type');
    }
  } else {
    return fs.readdirSync(manifestDir)
      .map((name) => path.join(manifestDir, name))
      .filter((p) => fs.existsSync(p) && fs.lstatSync(p).isFile())
      .map((manifest) => require(manifest));
  }
};
