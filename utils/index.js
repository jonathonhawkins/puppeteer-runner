const path = require('path');
const fs = require('fs');

module.exports = {
  consoleBlock: (...msg) => ((b = '////////////////////////') => console.log(`${b}\n`, ...msg, `\n${b}`))(),
  getScript: (script) => {
    const scriptPath = path.join(__dirname, '..', 'scripts', script);
    if (!fs.existsSync(scriptPath)) throw new Error(`Unable to find tasks with path: ${scriptPath}`);
    return require(scriptPath);
  },
  getDownloadData: (downloadDir) => (filename) => path.join(downloadDir, filename),
  getTestData: (filename) => path.join(__dirname, '..', 'data', filename),
  retry: async (fn, ms = 1000, maxRetry = 6) => {
    if (!fn) throw new Error('Retry failed');
    let count = 0;
    const r = async () => {
      count += 1;
      if (count === maxRetry) throw new Error('Retry failed');
      try {
        await fn();
        console.log('Retry successful');
      } catch (e) {
        console.log(`Retrying in ${ms}ms: ${e}`);
        await new Promise((res) => setTimeout(() => res(r()), ms));
      }
    };
    await r();
  },
  filenameGenerator: (base, mimeType, ...name) => {
    const timestamp = new Date().getTime();
    const filename = name.reduce(
      (acc, cur) => ((acc.length > 0) ? `${acc}_${cur}` : cur),
      ''
    );
    return `${base}/${timestamp}_${filename}.${mimeType}`;
  },
};
