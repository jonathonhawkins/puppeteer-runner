const fs = require('fs');
const path = require('path');
const merge = require('lodash.merge');
const yn = require('yn');

const mergeIfExists = (p, c = {}) => {
  if (!fs.existsSync(p)) return c;
  const x = require(p);
  return merge(c, x);
};

let config = {};

const defaultPath = path.join(__dirname, '..', 'config', 'default.js');
config = mergeIfExists(defaultPath, config);

const configPath = path.join(__dirname, '..', 'config', String(process.env.CONFIG));
config = mergeIfExists(configPath, config);

const environment = {
  config: process.env.CONFIG,
  mode: process.env.MODE,
  baseUrl: process.env.BASE_URL,
  multiStrategy: yn(process.env.MULTI_STRATEGY, { default: config.multiStrategy }),
  puppeteerConfig: {
    chromePath: process.env.CHROME_PATH,
    headless: yn(process.env.HEADLESS, { default: config.puppeteerConfig.headless }),
    devtools: yn(process.env.DEV_TOOLS, { default: config.puppeteerConfig.devtools }),
    slowMo: process.env.SLOW_MO,
  },
  auth: {
    ldap: {
      username: process.env.LDAP_USERNAME || process.env.LDAP_USER || process.env.LDAP_EMAIL,
      password: process.env.LDAP_PASSWORD || process.env.LDAP_PASS,
    },
    twitter: {
      username: process.env.TWITTER_USERNAME || process.env.TWITTER_USER || process.env.TWITTER_EMAIL,
      password: process.env.TWITTER_PASSWORD || process.env.TWITTER_PASS,
    },
  },
};
config = merge(config, environment);

// Note: I'm only logging specific values to ensure I don't expose sensitive data such as the auth object.
console.table(
  {
    'root config': {
      config: config.config,
      baseUrl: config.baseUrl,
      multiStrategy: config.multiStrategy,
    },
  },
);
console.table(
  {
    'puppeteer config': {
      chromePath: config.puppeteerConfig.chromePath,
      headless: config.puppeteerConfig.headless,
      devtools: config.puppeteerConfig.devtools,
    },
  },
);

module.exports = config;
