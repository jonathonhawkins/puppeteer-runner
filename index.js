const puppeteer = require('puppeteer');
const path = require('path');
const fs = require('fs');
const mkdirp = require('make-dir');
const dot = require('dot-object');
const {
  getTestData,
  getDownloadData,
  filenameGenerator,
  consoleBlock,
  getScript,
  retry,
} = require('./utils/index.js');
const config = require('./utils/config.js');
const manifests = require('./utils/manifests.js')(config);
const scriptUtils = require('./scripts/utils/index.js');

(async () => {
  console.table(manifests.map((m) => ({ manifest: m.meta.name })));
  for (const manifest of manifests) {
    console.group(manifest.meta.name);
    const runTimestamp = String(new Date().getTime());
    const runDir = path.join(__dirname, 'logs', runTimestamp);
    const dirs = {
      download: path.join(runDir, 'download'),
      screenshot: path.join(runDir, 'screenshot'),
      trace: path.join(runDir, 'trace'),
    };
    await Promise.all([
      mkdirp(dirs.download),
      mkdirp(dirs.screenshot),
      mkdirp(dirs.trace),
    ]);
    const browser = await puppeteer.launch({
      ...config.puppeteerConfig,
      /*
      * --- Note ---
      * this is for gitlabs pipeline env
      * see: https://github.com/Googlechrome/puppeteer/issues/290#issuecomment-322852784
      */
      defaultViewport: { width: 1200, height: 1000 },
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    }).catch((e) => {
      console.error('We failed to launch puppeteer:', e);
      process.exit(1);
    });
    const version = await browser.version();
    consoleBlock('Chrome version:', version);
    const page = await browser.newPage();
    try {
      const cdp = await page.target().createCDPSession();
      await cdp.send('Page.setDownloadBehavior', {
        behavior: 'allow',
        downloadPath: dirs.download,
      });
      const context = {
        meta: manifest.meta,
        page,
        navigationPromise: page.waitForNavigation(),
        getTestData,
        utils: scriptUtils,
        config,
        download: {
          wait: (filename, timeout = 1000) => retry(() => new Promise((res, rej) => {
            const download = path.join(dirs.download, filename);
            const exists = fs.existsSync(download);
            return (exists) ? res() : rej(new Error(`File not found: ${download}`));
          }), timeout),
          path: getDownloadData(dirs.download),
        },
        store: {},
      };
      await retry(() => page.goto(config.baseUrl, { waitUntil: 'domcontentloaded' }), 1000);
      for (const [name, step] of Object.entries(manifest.steps)) {
        console.group(name);
        const screenshotFilenameGenerator = (...operation) => filenameGenerator(dirs.screenshot, 'png', name, ...operation);
        await page.screenshot({ path: screenshotFilenameGenerator('pre-step') });
        const traceFilenameGenerator = (...operation) => filenameGenerator(dirs.trace, 'json', name, ...operation);
        const TRACE_OPTIONS = { path: traceFilenameGenerator() };
        await page.tracing.start(TRACE_OPTIONS);
        const url = await page.url();
        if (step.origin) {
          const origin = config.baseUrl + step.origin;
          if (url !== origin) {
            await page.goto(origin);
            await context.navigationPromise;
          }
        }
        if (typeof step.store !== 'undefined') {
          if (!Array.isArray(step.store)) {
            throw new Error('step.store is not an array');
          }
          step.store.forEach(({ key, value }) => {
            dot.str(key, value, context.store);
          });
        }
        if (typeof step.storeData !== 'undefined') {
          if (typeof step.data === 'undefined') {
            step.data = {};
          }
          Object.entries(step.storeData).forEach(([storeKey, dataKey]) => {
            dot.copy(storeKey, dataKey, context.store, step.data);
          });
        }
        const script = getScript(step.path);
        await script({
          ...context,
          step,
          screenshotFilenameGenerator,
          traceFilenameGenerator,
        });
        await page.tracing.stop(TRACE_OPTIONS);
        console.groupEnd(name);
      }
      consoleBlock(manifest.meta.name, 'Successful');
    } catch (generalException) {
      consoleBlock(manifest.meta.name, 'Failed');
      console.error(generalException);
      try {
        const url = await page.url();
        console.error('Error url:', url);
        await page.screenshot({ path: filenameGenerator(dirs.screenshot, 'png', 'error') });
      } catch (screenshotException) {
        console.error('Unable to screenshot the issue:', screenshotException);
      }
      process.exit(1);
    }
    if (browser) {
      await browser.close();
    }
    console.groupEnd(manifest.meta.name);
  }
})();
