// Note: configs can be either .json or .js as long as they return a json object
module.exports = {
  config: undefined, // the name of the config file that will overwrite this object
  target: undefined, // the name of the project we are targeting
  mode: undefined, //  the name of the manifest we will use
  baseUrl: 'http://localhost:3000', // the base url we will be targeting our requests at
  multiStrategy: false, //  if the target has multiple login strategies
  puppeteerConfig: {
    chromePath: undefined, // the path to the chrome executable
    headless: true, // if we want to run chrome in headless mode
    devtools: false, // if we want the dev tools to be shown during the run, note this overwrite headless to true
    slowMo: undefined, // slows down Puppeteer operations by the specified amount of milliseconds
  },
  auth: {
    ldap: {
      username: undefined,
      password: undefined,
    },
    twitter: {
      username: undefined,
      password: undefined,
    },
  },
};
