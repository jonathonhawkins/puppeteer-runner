module.exports = {
  selectDeleteAndType: async ({ page, selector, text }) => {
    if (selector && text) {
      const elementHandle = await page.$(selector);
      await elementHandle.click();
      await elementHandle.focus();
      // click three times to select all
      await elementHandle.click({ clickCount: 3 });
      await elementHandle.press('Backspace');
      await elementHandle.type(text);
    }
  },
};
